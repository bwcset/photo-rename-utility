﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Photo
    {
        public string currentPath { get; set; }
        public string newPath { get; set; }
        public string extension { get; set; }
        public string currentFileName { get; set; }
        public string newFileName { get; set; }
        public bool hasAvailableTags { get; set; }

        public Photo(string cp, string np, string ext, string cfn, string nfn, bool hat = false)
        {
            currentPath = cp;
            newPath = np;
            extension = ext;
            currentFileName = cfn;
            newFileName = nfn;
            hasAvailableTags = hat;
        }
    }
}
