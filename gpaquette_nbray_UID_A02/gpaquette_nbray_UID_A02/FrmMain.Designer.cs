﻿namespace WindowsFormsApp1
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.SelectFileBtn = new System.Windows.Forms.Button();
            this.SelectFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.pathlb = new System.Windows.Forms.Label();
            this.fileContentsListBox = new System.Windows.Forms.ListBox();
            this.exifFilterListbox = new System.Windows.Forms.CheckedListBox();
            this.ExifFilterlb = new System.Windows.Forms.Label();
            this.FrontRadio = new System.Windows.Forms.RadioButton();
            this.BackRadio = new System.Windows.Forms.RadioButton();
            this.tagPlacementGroupBox = new System.Windows.Forms.GroupBox();
            this.AddTagsBtn = new System.Windows.Forms.Button();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.filePreviewListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.replaceNameCheckBox = new System.Windows.Forms.CheckBox();
            this.FrmMainToolStrip = new System.Windows.Forms.ToolStrip();
            this.FileDropDown = new System.Windows.Forms.ToolStripDropDownButton();
            this.OpenFolderTool = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveTool = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAllTool = new System.Windows.Forms.ToolStripMenuItem();
            this.CloseTool = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ExcludeNullCheck = new System.Windows.Forms.CheckBox();
            this.ReplaceNameToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.ExcludeNullToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.ExifFilterToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.TagPlacementToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.FileContentToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.FilePreviewToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tagPlacementGroupBox.SuspendLayout();
            this.FrmMainToolStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SelectFileBtn
            // 
            this.SelectFileBtn.Location = new System.Drawing.Point(8, 422);
            this.SelectFileBtn.Name = "SelectFileBtn";
            this.SelectFileBtn.Size = new System.Drawing.Size(131, 23);
            this.SelectFileBtn.TabIndex = 0;
            this.SelectFileBtn.Text = "Open Folder";
            this.SelectFileBtn.UseVisualStyleBackColor = true;
            this.SelectFileBtn.Click += new System.EventHandler(this.SelectFileBtn_Click);
            // 
            // SelectFolderDialog
            // 
            this.SelectFolderDialog.Description = "Select a folder containing photos, which you would like to rename:";
            // 
            // pathlb
            // 
            this.pathlb.AutoSize = true;
            this.pathlb.Location = new System.Drawing.Point(6, 23);
            this.pathlb.Name = "pathlb";
            this.pathlb.Size = new System.Drawing.Size(35, 13);
            this.pathlb.TabIndex = 1;
            this.pathlb.Text = "Path: ";
            // 
            // fileContentsListBox
            // 
            this.fileContentsListBox.ColumnWidth = 180;
            this.fileContentsListBox.FormattingEnabled = true;
            this.fileContentsListBox.Location = new System.Drawing.Point(9, 36);
            this.fileContentsListBox.Name = "fileContentsListBox";
            this.fileContentsListBox.Size = new System.Drawing.Size(370, 381);
            this.fileContentsListBox.TabIndex = 2;
            // 
            // exifFilterListbox
            // 
            this.exifFilterListbox.CheckOnClick = true;
            this.exifFilterListbox.FormattingEnabled = true;
            this.exifFilterListbox.Items.AddRange(new object[] {
            "Camera Make",
            "Camera Model",
            "Creation Date",
            "User Comment",
            "Width_Height"});
            this.exifFilterListbox.Location = new System.Drawing.Point(6, 82);
            this.exifFilterListbox.Name = "exifFilterListbox";
            this.exifFilterListbox.Size = new System.Drawing.Size(138, 79);
            this.exifFilterListbox.TabIndex = 3;
            this.exifFilterListbox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.exifFilterListbox_ItemCheck);
            // 
            // ExifFilterlb
            // 
            this.ExifFilterlb.AutoSize = true;
            this.ExifFilterlb.Location = new System.Drawing.Point(3, 66);
            this.ExifFilterlb.Name = "ExifFilterlb";
            this.ExifFilterlb.Size = new System.Drawing.Size(60, 13);
            this.ExifFilterlb.TabIndex = 4;
            this.ExifFilterlb.Text = "EXIF Filters";
            // 
            // FrontRadio
            // 
            this.FrontRadio.AutoSize = true;
            this.FrontRadio.Checked = true;
            this.FrontRadio.Location = new System.Drawing.Point(6, 19);
            this.FrontRadio.Name = "FrontRadio";
            this.FrontRadio.Size = new System.Drawing.Size(49, 17);
            this.FrontRadio.TabIndex = 5;
            this.FrontRadio.TabStop = true;
            this.FrontRadio.Text = "Front";
            this.FrontRadio.UseVisualStyleBackColor = true;
            // 
            // BackRadio
            // 
            this.BackRadio.AutoSize = true;
            this.BackRadio.Location = new System.Drawing.Point(6, 43);
            this.BackRadio.Name = "BackRadio";
            this.BackRadio.Size = new System.Drawing.Size(50, 17);
            this.BackRadio.TabIndex = 6;
            this.BackRadio.TabStop = true;
            this.BackRadio.Text = "Back";
            this.BackRadio.UseVisualStyleBackColor = true;
            // 
            // tagPlacementGroupBox
            // 
            this.tagPlacementGroupBox.Controls.Add(this.FrontRadio);
            this.tagPlacementGroupBox.Controls.Add(this.BackRadio);
            this.tagPlacementGroupBox.Location = new System.Drawing.Point(6, 167);
            this.tagPlacementGroupBox.Name = "tagPlacementGroupBox";
            this.tagPlacementGroupBox.Size = new System.Drawing.Size(138, 73);
            this.tagPlacementGroupBox.TabIndex = 8;
            this.tagPlacementGroupBox.TabStop = false;
            this.tagPlacementGroupBox.Text = "Tag Placement";
            // 
            // AddTagsBtn
            // 
            this.AddTagsBtn.Enabled = false;
            this.AddTagsBtn.Location = new System.Drawing.Point(145, 422);
            this.AddTagsBtn.Name = "AddTagsBtn";
            this.AddTagsBtn.Size = new System.Drawing.Size(131, 23);
            this.AddTagsBtn.TabIndex = 9;
            this.AddTagsBtn.Text = "Preview Name Change";
            this.AddTagsBtn.UseVisualStyleBackColor = true;
            this.AddTagsBtn.Click += new System.EventHandler(this.AddTagsBtn_Click);
            // 
            // SaveBtn
            // 
            this.SaveBtn.Enabled = false;
            this.SaveBtn.Location = new System.Drawing.Point(655, 422);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(131, 23);
            this.SaveBtn.TabIndex = 10;
            this.SaveBtn.Text = "Save ";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // filePreviewListBox
            // 
            this.filePreviewListBox.ColumnWidth = 250;
            this.filePreviewListBox.FormattingEnabled = true;
            this.filePreviewListBox.HorizontalScrollbar = true;
            this.filePreviewListBox.Location = new System.Drawing.Point(385, 36);
            this.filePreviewListBox.Name = "filePreviewListBox";
            this.filePreviewListBox.Size = new System.Drawing.Size(370, 381);
            this.filePreviewListBox.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(382, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Preview:";
            // 
            // replaceNameCheckBox
            // 
            this.replaceNameCheckBox.AutoSize = true;
            this.replaceNameCheckBox.Location = new System.Drawing.Point(6, 19);
            this.replaceNameCheckBox.Name = "replaceNameCheckBox";
            this.replaceNameCheckBox.Size = new System.Drawing.Size(154, 17);
            this.replaceNameCheckBox.TabIndex = 13;
            this.replaceNameCheckBox.Text = "Replace Original File Name";
            this.replaceNameCheckBox.UseVisualStyleBackColor = true;
            this.replaceNameCheckBox.CheckedChanged += new System.EventHandler(this.ReplaceNameCheckBox_CheckedChanged);
            // 
            // FrmMainToolStrip
            // 
            this.FrmMainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileDropDown,
            this.toolStripSeparator1,
            this.toolStripDropDownButton1});
            this.FrmMainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.FrmMainToolStrip.Name = "FrmMainToolStrip";
            this.FrmMainToolStrip.Size = new System.Drawing.Size(931, 25);
            this.FrmMainToolStrip.TabIndex = 15;
            this.FrmMainToolStrip.Text = "toolStrip1";
            // 
            // FileDropDown
            // 
            this.FileDropDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.FileDropDown.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenFolderTool,
            this.SaveTool,
            this.clearAllTool,
            this.CloseTool});
            this.FileDropDown.Image = ((System.Drawing.Image)(resources.GetObject("FileDropDown.Image")));
            this.FileDropDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FileDropDown.Name = "FileDropDown";
            this.FileDropDown.Size = new System.Drawing.Size(38, 22);
            this.FileDropDown.Text = "File";
            // 
            // OpenFolderTool
            // 
            this.OpenFolderTool.Image = global::WindowsFormsApp1.Properties.Resources.OpenFolder_16x;
            this.OpenFolderTool.Name = "OpenFolderTool";
            this.OpenFolderTool.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.OpenFolderTool.Size = new System.Drawing.Size(182, 22);
            this.OpenFolderTool.Text = "&Open Folder";
            this.OpenFolderTool.Click += new System.EventHandler(this.OpenFolderTool_Click);
            // 
            // SaveTool
            // 
            this.SaveTool.Enabled = false;
            this.SaveTool.Image = global::WindowsFormsApp1.Properties.Resources.Save_16x;
            this.SaveTool.Name = "SaveTool";
            this.SaveTool.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.SaveTool.Size = new System.Drawing.Size(182, 22);
            this.SaveTool.Text = "&Save";
            this.SaveTool.Click += new System.EventHandler(this.SaveTool_Click);
            // 
            // clearAllTool
            // 
            this.clearAllTool.Enabled = false;
            this.clearAllTool.Name = "clearAllTool";
            this.clearAllTool.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.C)));
            this.clearAllTool.Size = new System.Drawing.Size(182, 22);
            this.clearAllTool.Text = "Clear All";
            this.clearAllTool.Click += new System.EventHandler(this.clearAllToolStripMenuItem_Click);
            // 
            // CloseTool
            // 
            this.CloseTool.Image = global::WindowsFormsApp1.Properties.Resources.Close_16x;
            this.CloseTool.Name = "CloseTool";
            this.CloseTool.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.CloseTool.Size = new System.Drawing.Size(182, 22);
            this.CloseTool.Text = "Close";
            this.CloseTool.Click += new System.EventHandler(this.CloseTool_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(45, 22);
            this.toolStripDropDownButton1.Text = "Help";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Image = global::WindowsFormsApp1.Properties.Resources.Question_16x;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // ClearBtn
            // 
            this.ClearBtn.Enabled = false;
            this.ClearBtn.Location = new System.Drawing.Point(792, 422);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(131, 23);
            this.ClearBtn.TabIndex = 16;
            this.ClearBtn.Text = "Clear All";
            this.ClearBtn.UseVisualStyleBackColor = true;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ExcludeNullCheck);
            this.groupBox1.Controls.Add(this.exifFilterListbox);
            this.groupBox1.Controls.Add(this.ExifFilterlb);
            this.groupBox1.Controls.Add(this.tagPlacementGroupBox);
            this.groupBox1.Controls.Add(this.replaceNameCheckBox);
            this.groupBox1.Location = new System.Drawing.Point(761, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(162, 249);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rename File Options";
            // 
            // ExcludeNullCheck
            // 
            this.ExcludeNullCheck.AutoSize = true;
            this.ExcludeNullCheck.Location = new System.Drawing.Point(6, 43);
            this.ExcludeNullCheck.Name = "ExcludeNullCheck";
            this.ExcludeNullCheck.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ExcludeNullCheck.Size = new System.Drawing.Size(122, 17);
            this.ExcludeNullCheck.TabIndex = 14;
            this.ExcludeNullCheck.Text = "Exclude NULL Tags";
            this.ExcludeNullCheck.UseVisualStyleBackColor = true;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 452);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ClearBtn);
            this.Controls.Add(this.FrmMainToolStrip);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.filePreviewListBox);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.AddTagsBtn);
            this.Controls.Add(this.fileContentsListBox);
            this.Controls.Add(this.pathlb);
            this.Controls.Add(this.SelectFileBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.Text = "Exif Photo Organizer";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.tagPlacementGroupBox.ResumeLayout(false);
            this.tagPlacementGroupBox.PerformLayout();
            this.FrmMainToolStrip.ResumeLayout(false);
            this.FrmMainToolStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SelectFileBtn;
        private System.Windows.Forms.FolderBrowserDialog SelectFolderDialog;
        private System.Windows.Forms.Label pathlb;
        private System.Windows.Forms.ListBox fileContentsListBox;
        private System.Windows.Forms.CheckedListBox exifFilterListbox;
        private System.Windows.Forms.Label ExifFilterlb;
        private System.Windows.Forms.RadioButton FrontRadio;
        private System.Windows.Forms.RadioButton BackRadio;
        private System.Windows.Forms.GroupBox tagPlacementGroupBox;
        private System.Windows.Forms.Button AddTagsBtn;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.ListBox filePreviewListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox replaceNameCheckBox;
        private System.Windows.Forms.ToolStrip FrmMainToolStrip;
        private System.Windows.Forms.ToolStripDropDownButton FileDropDown;
        private System.Windows.Forms.ToolStripMenuItem OpenFolderTool;
        private System.Windows.Forms.ToolStripMenuItem SaveTool;
        private System.Windows.Forms.ToolStripMenuItem CloseTool;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem clearAllTool;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox ExcludeNullCheck;
        private System.Windows.Forms.ToolTip ReplaceNameToolTip;
        private System.Windows.Forms.ToolTip ExcludeNullToolTip;
        private System.Windows.Forms.ToolTip ExifFilterToolTip;
        private System.Windows.Forms.ToolTip TagPlacementToolTip;
        private System.Windows.Forms.ToolTip FileContentToolTip;
        private System.Windows.Forms.ToolTip FilePreviewToolTip;
    }
}

