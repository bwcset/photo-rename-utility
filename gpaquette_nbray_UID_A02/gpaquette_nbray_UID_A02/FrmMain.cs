﻿/*
 File: FrmMain.cs
 Developer: Gabriel Paquette, Nathan Bray
 Date: Nov, 1, 2018
 Description: This program is a file remaing program. It reads in jpg, or jpeg files, and
              renames them according to EXIF filters selected by the user.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace WindowsFormsApp1
{

    /*
    Name: FrmMain
    Developer: Gabriel Paquette, Nathan Bray
    Date: Nov 1, 2018
    Description: The form in which the program runs.
    */
    public partial class FrmMain : Form
    {

        string currentWorkingPath = "";
        Dictionary<string, int> filterValues = new Dictionary<string, int>();
        Dictionary<string, string> updatedFileNames = new Dictionary<string, string>();

        /*
        Name: FrmMain
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: The contructor for form main.
        */
        public FrmMain()
        {
            InitializeComponent();
        }

        /*
        Name: FrmMain_Load
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: This function is called when the main form loads. It sets the tool tip descriptions.
        */
        private void FrmMain_Load(object sender, EventArgs e)
        {
            ReplaceNameToolTip.SetToolTip(this.replaceNameCheckBox, "Replaces the original filename with a newly generated \nfilename consisting of formated EXIF data tags.");
            ReplaceNameToolTip.SetToolTip(this.ExcludeNullCheck, "Excludes NULL tags when no EXIF data is found for any checked EXIF filters.");
            ReplaceNameToolTip.SetToolTip(this.exifFilterListbox, "Select which EXIF tags should be added to the new file name.\nIf no EXIF data is found, 'NULL' will be used instead.");
            ReplaceNameToolTip.SetToolTip(this.tagPlacementGroupBox, "Determinds the placement of the tags for the new file name.");
            ReplaceNameToolTip.SetToolTip(this.fileContentsListBox, "Displays the files that will be renamed.");
            ReplaceNameToolTip.SetToolTip(this.filePreviewListBox, "Displays a preview of the new file names.");

            filterValues.Add("Camera Make", 0x010F);
            filterValues.Add("Camera Model", 0x0110);
            filterValues.Add("Creation Date", 0x0132);
            filterValues.Add("User Comment ", 0x9286);

        }

        /*
        Name: SelectFileBtn_Click
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Opens a folder dialog where the user can select a file to open. 
                     The files are then read in and populated in the file content listbox
        */
        private void SelectFileBtn_Click(object sender, EventArgs e)
        {
            using (SelectFolderDialog)
            {
                DialogResult result = SelectFolderDialog.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(SelectFolderDialog.SelectedPath))
                {
                    ClearControls();
                    currentWorkingPath = SelectFolderDialog.SelectedPath;
                    List<string> extensions = new List<string> { "jpg", "jpeg" };
                    string[] files = GetFilesWithExtensions(currentWorkingPath, extensions);

                    
                    clearAllTool.Enabled = true;
                    ClearBtn.Enabled = true;
                    pathlb.Text = "Path: " + currentWorkingPath;
                    
                    foreach (string file in files)
                    {
                        fileContentsListBox.Items.Add(Path.GetFileName(file));
                    }

                    if(exifFilterListbox.CheckedItems.Count > 0)
                    {
                        AddTagsBtn.Enabled = true;

                    }
                }
            }
        }


        /*
        Name: ClearControls
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Clears the controls and file data, so the user can start over.
        */
        private void ClearControls()
        {
            pathlb.Text = "Path:";
            fileContentsListBox.Items.Clear();
            filePreviewListBox.Items.Clear();
            currentWorkingPath = "";
            updatedFileNames.Clear();

        }


        /*
        Name:  EnableControls
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Enables some controls on the main form.
        */
        private void EnableControls()
        {
            pathlb.Text = "Path: " + currentWorkingPath;
            AddTagsBtn.Enabled = true;
            SaveBtn.Enabled = true;
            ClearBtn.Enabled = true;
            SaveTool.Enabled = true;
            clearAllTool.Enabled = true;
        }


        /*
        Name: DisableControls
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Disables some controls on the main form
        */
        private void DisableControls()
        {
            SaveBtn.Enabled = false;
            ClearBtn.Enabled = false;
            AddTagsBtn.Enabled = false;
            SaveTool.Enabled = false;
            clearAllTool.Enabled = false;
        }


        /*
        Name: GetFilesWithExtensions
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Gets all files in a folder that have a specifed extention
        */
        private string[] GetFilesWithExtensions(string path, List<string> extensions)
        {
            string[] allFilesInFolder = Directory.GetFiles(path);
            return allFilesInFolder.Where(f => extensions.Contains(f.Split('.').Last())).ToArray();
        }


        /*
        Name: AddTagsBtn_Click
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Adds all exif tags to file and adds the new file name to the preview list box
        */
        private void AddTagsBtn_Click(object sender, EventArgs e)
        {
            if (currentWorkingPath != "" && exifFilterListbox.CheckedItems.Count > 0)
            {
                updatedFileNames.Clear();
                filePreviewListBox.Items.Clear();

                foreach (string path in Directory.EnumerateFiles(currentWorkingPath, "*.jpg"))
                {
                    Photo photo = new Photo(path, "", Path.GetExtension(path), Path.GetFileNameWithoutExtension(path), "", false);

                    string fileTags = GenerateFileTags(photo);
                    GenerateNewFileNameAndPath(photo, fileTags);
                    StoreCurrentAndNewFileName(photo);
                    
                    //add the updated file name to the preview
                    filePreviewListBox.Items.Add(photo.newFileName + photo.extension);

                }
                EnableControls();
            }
        }


        /*
        Name:  StoreCurrentAndNewFileName
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Stores the current file and and the new file name for when the user wants to save at a later point.
        */
        private void StoreCurrentAndNewFileName(Photo photo)
        {
            //save the original file name and the new path, update the existing new path with the new path
            if (updatedFileNames.ContainsKey(photo.currentFileName))
            {
                updatedFileNames[photo.currentFileName] = photo.newFileName;
            }
            else // create a new entry
            {
                if (updatedFileNames.ContainsValue(photo.newFileName)) // if a new entry shares a name with an existing file, add a suffix to the end of the file.
                {
                    for (int i = 1; i < int.MaxValue; i++)
                    {
                        string tempNewFileName = photo.newFileName + "_" + i;

                        if (!updatedFileNames.ContainsValue(tempNewFileName))
                        {
                            updatedFileNames.Add(photo.currentFileName, tempNewFileName);
                            photo.newFileName = tempNewFileName;
                            break;
                        }
                    }
                }
                else
                {
                    updatedFileNames.Add(photo.currentFileName, photo.newFileName);
                }
            }
        }


        /*
        Name:  GenerateNewFileNameAndPath
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Generates the new file name for a file, and creates the new path asewll.
        */
        private void GenerateNewFileNameAndPath(Photo photo, string fileTags)
        {
            if (photo.hasAvailableTags)
            {
                if (replaceNameCheckBox.Checked) //replace the existing fileName
                {
                    photo.newFileName = fileTags;
                }
                else //add the tags to the front or the back of the filename
                {
                    if (FrontRadio.Checked) //fileName_tags
                    {
                        photo.newFileName = fileTags + "_" + photo.currentFileName;
                    }
                    else if (BackRadio.Checked) //tags_fileName
                    {
                        photo.newFileName = photo.currentFileName + "_" + fileTags;
                    }
                }
                photo.newPath = photo.currentPath.Replace(photo.currentFileName, photo.newFileName);
            }
            else // the file had no takes, and the file name stays the same
            {
                photo.newPath = photo.currentPath;
                photo.newFileName = photo.currentFileName;
            }
        }


        /*
        Name:  GenerateFileTags
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Generates the exif tags that will be added to the existing file's name.
        */
        private string GenerateFileTags(Photo photo)
        {
            string exifValue = "";
            string fileTags = "";
            int filterCode = 0;
            
            for (int i = 0; i < exifFilterListbox.CheckedItems.Count; i++)
            {
                exifValue = "NULL"; //set as a default incase the exif data doesn't exist.

                try
                {
                    Image image = Image.FromFile(photo.currentPath);

                    switch (exifFilterListbox.CheckedItems[i].ToString())
                    {
                        case "Camera Make":
                        case "Camera Model":
                        case "Creation Date":
                        case "User Comment":
                            filterCode = filterValues[exifFilterListbox.CheckedItems[i].ToString()];
                            //get raw exif value
                            exifValue = Encoding.UTF8.GetString(image.GetPropertyItem(filterCode).Value).Replace("\0", string.Empty);
                            //format exif value for easier reading
                            exifValue = FormatExifValue(exifValue, filterCode);
                            break;
                        case "Width_Height":
                            //W10_H30
                            exifValue = "W" + image.Width.ToString() + "_H" + image.Width.ToString();
                            break;
                    }
                    photo.hasAvailableTags = true;
                    image.Dispose();
                    
                }
                catch(Exception e)
                {
                    Console.WriteLine("No Exif Data for " + filterCode);
                }

                // if the exclude null check is checked, and the exif value is still NUll, don't add it to the file tags
                if (ExcludeNullCheck.Checked && exifValue == "NULL") 
                {
                    continue;
                }
                else
                { 
                    if (i == exifFilterListbox.CheckedItems.Count - 1)
                    {
                        fileTags += exifValue;
                    }
                    else
                    {
                        fileTags += exifValue + "_";
                    }
                }
                
            }

            return fileTags;
        }


        /*
        Name: FormatExifValue
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Takes the full exif value and formats it so the value is easier to read in a file name.
        */
        private string FormatExifValue(string exifValue, int filterCode)
        {
            switch (filterCode)
            {
                case 0x010F: //make
                case 0x0110: //model
                    if (exifValue.Length > 5) //cut the model down to 4 characters
                    {
                        exifValue = exifValue.Substring(0, 5).Trim();
                    }
                    break;
                case 0x0132: //date
                    DateTime exifDate;
                    if (DateTime.TryParseExact(exifValue, "yyyy:MM:dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out exifDate))
                    {
                        exifValue = exifDate.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        exifValue = "No_Date";
                    }
                    break;
                case 0x9286: //comment
                    if (exifValue.Length > 8) //cut the model down to 4 characters
                    {
                        exifValue = exifValue.Substring(0, 8).Trim();
                    }
                    break;
            }

            return exifValue;
        }


        /*
        Name: SaveBtn_Click
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Saves the preview names over the exisiting files names.
        */
        private void SaveBtn_Click(object sender, EventArgs e)
        {

            DialogResult dr = MessageBox.Show("Do you want to rename all files in the current folder?", "Rename Files Conformation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);

            if (dr == DialogResult.Yes)
            {
                foreach (KeyValuePair<string, string> file in updatedFileNames)
                {
                    //path,   newpath
                    File.Move(currentWorkingPath + file.Key + ".jpg", file.Value + ".jpg");
                }
            }
            
        }


        /*
        Name: ReplaceNameCheckBox_CheckedChanged
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Enables and disables the tag placement fields if this replacename check box is checked or not.
        */
        private void ReplaceNameCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (replaceNameCheckBox.Checked)
            {
                tagPlacementGroupBox.Enabled = false;
            }
            else
            {
                tagPlacementGroupBox.Enabled = true;
            }
        }


        /*
        Name:  SaveTool_Click
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Calls the save function when clicked from the tool bar
        */
        private void SaveTool_Click(object sender, EventArgs e)
        {
            SaveBtn_Click(sender, e);
        }


        /*
        Name:  CloseTool_Click
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Closes the program.
        */
        private void CloseTool_Click(object sender, EventArgs e)
        {
            FrmMain.ActiveForm.Close();
        }


        /*
        Name: OpenFolderTool_Click 
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Calls the open folder function when clicked from the tool bar.
        */
        private void OpenFolderTool_Click(object sender, EventArgs e)
        {
            SelectFileBtn_Click(sender, e);
        }


        /*
        Name:  clearAllToolStripMenuItem_Click
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Clears the list boxes and disables the controls when the user clicks clear all.
        */
        private void clearAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearBtn_Click(sender, e);
        }


        /*
        Name: ClearBtn_Click
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Clears the list boxes and disables the controls when the user clicks clear all.
        */
        private void ClearBtn_Click(object sender, EventArgs e)
        {
            ClearControls();
            DisableControls();
        }


        /*
        Name: helpToolStripMenuItem_Click
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: Opens a help message box when the user clicks help from the menu.
        */
        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Instructions:\n\n1) Import a folder by clicking 'Open Folder'.\n2)Select which Exif tags you would like to add to the file name.\n" +
                "You can choose if the tags get appended to the front or the back of the original file name.\nOr you can also choose if the file name will be " +
                "removed in the renaming process.\n3)Click 'Preview New Names' to see the updated names.\n4)Click save to rename the files.", "File Rename Helper");
        }


        /*
        Name: exifFilterListbox_ItemCheck
        Developer: Gabriel Paquette, Nathan Bray
        Date: Nov 1, 2018
        Description: This function enables the preview filename btn if the user has any exif filters selected.
        */
        private void exifFilterListbox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //a files has to be loaded 
            if (currentWorkingPath != "" && fileContentsListBox.Items.Count > 0)
            {
                //checks if any filters are checked
                if (exifFilterListbox.CheckedItems.Count == 1 && e.NewValue == CheckState.Unchecked)
                {
                    AddTagsBtn.Enabled = false;
                }
                else // a filter is selected
                {  
                    AddTagsBtn.Enabled = true;
                }
            }
        }



    }
}
